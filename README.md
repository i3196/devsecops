# Desafio DevOps
O objetivo desse desafio é analisar o raciocínio lógico na implantação de uma aplicação em contêiner, esse ambiente deve seguir algumas especificações dos recursos necessário, sua entrega deve estar automatizada em uma pipeline de CI/CD, serão avaliados a qualidade de código e sua autoria na escrita. Sinta-se à vontade para incluir referência de boas práticas em comentários.  

## Requisitos
* Escrever Dockerfile simples com uma página estática "Hello World".
* Provisonar infraestrua como código usando terraform.
* Usar recursos AWS (ECS, ECR, ALB) use outros recursos que julgar necessário.
* No Gitlab o arquivo `.gitlab-ci.yml` deve conter as seguintes etapas (Build, Release, Deploy).

> **Dica**: Leve em consideração que seria um ambiente muito próximo de produção, portanto, escalabilidade, billing, segurança, tags, monitoramento e logging serão avaliados.

## Forma de entrega
Realizar um fork do nosso repositório e nos envie a url do seu projeto, seguindo os requisitos apresentados. O arquivo `ANSWERS.md` pode ser usado para escrever instruções técnicas de como executar seu código, queremos que você crie uma forma simples que possamos criar toda sua infraestrutura em nossa conta.

#### O que esperamos na entrega:
1. Dockerizar aplicação web simples.
2. Escrever pipeline CI/CD no GitlabCI.
    * Na estrátegia de deployment considere usar canary ou blue/green.
3. Implementar infraestrutura como código com o terraform na AWS, **não precisa compartilha os segredos**.

**Desejamos boa sorte no processo seletivo.**
